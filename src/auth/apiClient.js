import axios from 'axios';

export default class ApiClient {
  constructor() {
    this.api = 'https://demo-travelize.buspaket.de/api/currency_exchanges';
  }

  fetch(url, config, callback) {
    fetch(url, config).then(response => {
      if (response.ok) {
        return response.text();
      }
      else {
        throw new Error(response.statusText);
      }
    })
    .then(text => {
      try {
        const data = JSON.parse(text);
        callback(null, data);
      } catch (err) {
        callback(null, text);
      }
    })
    .catch((err) => {
      callback(err, null);
    });
  }

  request(method, url, body, callback) {

    const getConfig = () => {
      const config = {
        method: method,
        url: url,
        headers: {
          "Authorization": "Bearer eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX0FETUlOIiwiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiTGF1cmVueiIsImlhdCI6MTU3Njg5NTEyNywiZXhwIjoxNTc3NDk5OTI3fQ.yph8Gj_inVUClAb7zwAz6WgbB7HQAdMNmL1GoTzb1ZY_UdqXtN1MsP-ITC4kKz_IWmnWkVYiFFr0ctPrrRgQycPnuYMMtFlRIZ7GFml-Z43CT71Udj_zx9L1mgiy7fned2BPEbNXWxID8B-HODWebg5kSPv7RNPY5QXSHW8vPDX5QOrY8iQ7tuEKf3B1QjHXOCixD7CPnB0ReNFGuv7niBlVUN5RT-z9ct51ODjwMF2G4rLeT9Fdg3mO1wRT_G0zFtIOXS7pSyWn-r9RqaQLwEhfUIhUpDkUklHz7FMIq4Rw7uquQdijjPkuCnbeQ-OsAA5y0Cj0ZAPzZZmATwymM0s25fLQMGtB1fTxDayYZGp1c8W_qTqAjpPhbXvmA3y9b-FaNmJOQw7V_a4BzkB-GMOLQxXFnzm9FcivQw5j8KeEekJtrDewIuDJ03xSVdht8gD8hyWw58bfSVv1Tv9-f-H2s4UL9BwXRNoacSIJLxSaFiVAXwSzw10mr7wokcV1s_7rVf_-U9gyErrTJuCzYct41byttw3f5bZByGwPPCFAhogTIswyClCL9_UbQg9rKZYWoPb-mpg5tIPHSjpXLWSWXUzj6Y3rRP-HpLz3K6SZnIu-PXYiaFYzdFfMH-ea5uu1EcTXnu5sYvne7LsoMFZGJ_nz1_DeKwx8uZwa7mA",
        }
      }
      if (method == "POST") {
        config.data = body,
        config.headers["Content-Type"] = "application/json"
      }
      return config;
    }

    const log = (error) => {
      if (error && error.response && error.response.data) {
        console.log(error.response.data);
      } else {
        console.log(error);
      }
    }

    axios(getConfig()).then((response) => {
      callback(response.data);
    })
    .catch((error) => {
      log();
    });
  }

  get(url, callback) {
    this.request("GET", url, null, (response) => callback(response));
  }

  post(url, body, callback) {
    this.request("POST", url, body, (response) => callback(response));
  }

  put(url, body, callback) {
    this.request("PUT", url, body, (response) => callback(response));
  }

  delete(url, body, callback) {
    this.request("DELETE", url, body, (response) => callback(response));
  }
}
import VueRouter from 'vue-router';

const routes = [
];

export default new VueRouter({
  mode: 'history',
  routes: routes,
  scrollBehavior: function (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});
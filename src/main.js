import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

import MultiVue from "vue-multivue";
import router from './routing/main';
import store from './store/index';

// components
import ExchangeRates from './components/ExchangeRates.vue';
import ContextMenu from './components/context/ContextMenu.vue';

// include flag css
import './../assets/css/main.scss';
import './../vendor/tel/css/intlTelInput.css';

Vue.config.productionTip = false;
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.component('ContextMenu', ContextMenu)

new MultiVue(".vue-app-exchangerates", {
  store,
  router: router,
  components: {
    'exchangerates': ExchangeRates
  }
});
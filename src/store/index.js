import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(Vuex);
Vue.use(VueAxios, axios);

import ApiClient from './../auth/apiClient.js'

const store = new Vuex.Store({
  state: {
    tpIndex: null,
    index: null,
    data: null
  },

  actions: {

    // call API
    loadData({ comit }) {
      var apiClient = new ApiClient();
      apiClient.get(apiClient.api, response => {
        console.log(response);
        this.state.data = response;
      });
    }
  }
});
  
export default store;
